(function() {

	var ChromeKanddy = {
		injectable : function() {
			// outerWidth for background tab is 0 in chrome so injection doesnt completes
			// hence set outerWidth if its 0
			var outerWidth = window.outerWidth == 0 ? screen.width : window.outerWidth;
			// window size should be more than 75% of monitor size
			if(outerWidth > (screen.width * 3 / 4))
				return document.location.protocol == "http:";
			else
				return false;
		},
		loader : function(url, callback) {
			var ajaxLib;
			ajaxLib = document.createElement('script');
			ajaxLib.setAttribute('type', 'text/javascript');
			ajaxLib.onload = callback;
			ajaxLib.setAttribute('src', url);
			document.head.appendChild(ajaxLib);
		},
		cacheBuster : function() {
			var t = new Date();
			var buster = "?buster=" + currentTime.getHours() + currentTime.getDate() + currentTime.getMonth() + currentTime.getFullYear();

		},
		init : function() {
			if(ChromeKanddy.injectable() == true) {				
				ChromeKanddy.loader(chrome.extension.getURL('jquery-1.7.1.min.js'), function() {
					debugger
				});
			}
		}
	}
	ChromeKanddy.init();

})();
